Russian

Позволяет создавать ЧПУ для терминов и нод, к которым добавлены данные термины.

После включении модуля на странице добавления/редактирования термина появится дополнительное поле "Synonym",
в котором необходимо указать синоним для термина. При заполнении имени термина, поле "Synonym" автоматически
заполняется (используется Google Translate Api). Синоним может быть использован для создания человеко-понятных 
адресов терминов или нод на странице "patterns" модуля Pathauto. На этой странице будут доступны два новых шаблона:
1) [node:node-synonym-path] для настройки адресов нод;
2) [term:term-synonym-path] для настройки адресов терминов.

Если термин имеет несколько родителей, то алиас будет создан на основе первого родителя, идущего в списке.
Если нода имеет несколько словарей/терминов, как и в случае с терминами, алиас будет создан на основе первого термина 
первого словаря.

Пример использования:
1) В настройках pathauto для Taxonomy term paths для словаря "Catalog" указываем шаблон: 
catalog/[term:term-synonym-path].
Для типа материала, к которому добавлен данный словарь, указываем шаблон
catalog/[node:node-synonym-path]/product[node:nid].html
2) Добавляем термин в словарь "Catalog": "мебель". В поле "Synonym" автоматически появится перевод "furniture". Сохраняем.
Адрес для термина "мебель" будет http://SITE/catalog/furniture
3) Добавляем материал. Выбираем термин "мебель". Адрес материала будет http://SITE/catalog/furniture/product1.html
Если не выбрать термин, то адрес страницы будет http://SITE/catalog/product1.html

Для работы требуются следующие модули:
1) Taxonomy
2) Pathauto
3) Token

English

Allows you to create human-readable URLs for terms and nodes.
After module is switched on the page adding/editing of the term will be available field "Synonym",
using this field you can specify a synonym. When filling in the name of the term, the field "Synonym" is automatically
filled (using Google Translate Api). Synonym can be used to create human-readable URLs for terms or nodes on the page "patterns" of 
module Pathauto. On this page will be available two new patterns:
1) [node:node-synonym-path] to configure alias for nodes;
2) [term:term-synonym-path] to configure alias for terms.

If term has several parents, then alias will be created from the first parent, going on the list.
If node has several vocabularies/terms, as is the case with terms, alias will be created on the basis of the first term
the first vocabulary.

Example of use:
1) In the setting of pathauto for Taxonomy term paths for the vocabulary "Catalog" indicate a pattern:
catalog/[term:term-synonym-path].
For the type of node to which is added the vocabulary, specify a pattern
catalog/[node:node-synonym-path]/product[node:nid].html
2) Add a term to the vocabulary "Catalog": "мебель" (russian language). In the "Synonym" will appear automatically translate "furniture". Save.
The alias for the term "furniture" will http://SITE/catalog/furniture
3) Add material. Choose the term "мебель". Alias material will http://SITE/catalog/furniture/product1.html
If you do not choose a term, then the page address will http://SITE/catalog/product1.html

Dependencies:
1) Taxonomy
2) Pathauto
3) Token