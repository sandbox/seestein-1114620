// $Id$

(function ($) {
  Drupal.behaviors.synonym_term = {
    attach: function(context, settings) {
      $('#edit-name').keyup(function() {
    	$.ajax({
    	  type: 'POST',
    	  url: '/synonym_term/translate',
    	  beforeSend: function(){
    		     
    	  },
    	  success: function(data) {
    		$("#edit-synonym").val(data);
    	  },
    	  dataType: 'html',
    	  data: [{name:"sourcetext",value:$("#edit-name").val()}]
    	});
      });	
	  
    }
  };

})(jQuery);